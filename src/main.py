from PIL import Image
import math
import sys

size = 200
list_colors = [(255,0,0),(0,255,0),(0,0,255),(255,255,0),(0,255,255)]
points = [(10,10),(160,160),(60,100),(50,50),(100,60)]

def getRange(start, end):
	tmp = (start[0] - end[0])*(start[0]-end[0])+(start[1]-end[1])*(start[1]-end[1])
	return math.sqrt(tmp)

def getColor(x, y):
	currColor = 0
	minRange = sys.maxint
	for i in range(5):
		if points[i] == (x,y):
			return(0,0,0)
		dist = getRange((x,y), points[i])
		if dist < minRange:
			minRange = dist
			currColor = i
	return list_colors[currColor]


im = Image.new('RGB',(size,size),color=0)
for y in range(size) :
	for x in range(size) :
		color = getColor(x, y)
		im.putpixel((x,y), color)

im.save('testing.png')
